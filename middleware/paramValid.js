// const wrong = require('../share/module/index').wrong
const error = require('../error')
function checkParam(param, reqParam) {
	if (param.required && param.required.length > 0) {
		for (let i = 0; i < param.required.length; i++) {
			if (!Object.prototype.hasOwnProperty.call(reqParam, param.required[i]) || reqParam[param.required[i]] == null) {
				throw error.parameterIsWrong
			}
		}
	}
	if (param.properties) {
		let keys = Object.keys(param.properties)
		for (let i = 0; i < keys.length; i++) {
			let key = keys[i]
			if (Object.prototype.hasOwnProperty.call(reqParam, key)) {
				if (param.properties[key].type == 'array') {
					if (reqParam[key].constructor != Array) {
						throw error.parameterIsWrong
					}
				} else if (param.properties[key].type != typeof reqParam[key]) {
					throw error.parameterIsWrong
				}
				if (param.properties[key].enum && param.properties[key].enum.length > 0) {
					let enums = param.properties[key].enum
					if (!enums.includes(reqParam[key])) {
						throw error.parameterIsWrong
					}
				}
			}
		}
	}
}
exports.paramValid = {
	order: 4,
	runner: async function (meta, req, res, next) {
		try {
			if (meta.param) {
				if (meta.param.query) {
					checkParam(meta.param.query, req.query)
				}
				if (meta.param.headers) {
					checkParam(meta.param.headers, req.headers)
				}
				if (meta.param.body) {
					checkParam(meta.param.body, req.body)
				}
				if (meta.param.params) {
					checkParam(meta.param.params, req.params)
				}
				next()
			} else {
				next()
			}
		} catch (err) {
			next(err)
		}
	}
};