const LoggerHandler = require('../module/loggerHandler')
const config = require('../config/setting')
const util = require('../utils/utility')
const classPath = 'middleware/log.js'

exports.log = {
	order: 0,
	runner: function (meta, req, res, next) {
		try {
			let requestId = util.generatUUIDV1().replace(/-/g, '')
			req.requestId = requestId
			let logger = new LoggerHandler(config.loggerName, { requestId })
			res.logger = logger
			let method = req.method
			let originalUrl = req.originalUrl
			let apiKey = this.apiKey
			let headers = req.headers
			let params = req.params
			let query = req.query
			let clientIp = req.clientIp
			let body = req.body
			let fields = Object.assign({ apiKey, method, originalUrl, clientIp }, headers, query, params, body)
			logger.info('API_START', classPath, 'log.runner', fields)
			next()
		} catch (err) {
			next(err)
		}
	}
}