"use strict";

require('../base');
let Mail = require('../../data/mail_template');

let MailStatic = function (model) {
	let self = this;
	this.list = [];
	this.mail = Mail;
	this.model = model;

	this.load = function () {
		this.model.Mail_template.destroy({
			where: {}
		}).then(function () {
			Object.keys(self.mail).forEach(function (key) {
				let elem = {
					'id': self.mail[key].Id,
					'name': self.mail[key].Subject,
					'template': self.mail[key].Body
				}

				self.model.Mail_template.create(elem
				).catch(function (err) {
					console.log(err);
				});
			});
		}).catch(function (err) {
			console.log(err);
		});
	}
};

module.exports = MailStatic;