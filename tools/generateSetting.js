"use strict";

let	ModelManage = require('../module/modelManage'),
	Mysql = require('../module/mysqlClient').pools,
	config = require('../config/setting');

let ErrorStatic = require('./static/error');
let MailStatic = require('./static/mail');
let ThirdPartyStatic = require('./static/third_party');
const command = process.argv[2];

let mysql  = new Mysql(config);
let model  = new ModelManage(mysql.sequelize);

(function() {
	if (command == 'start') {
		new ErrorStatic(model).load();
		new MailStatic(model).load();
		new ThirdPartyStatic(model).load();
	}
})();
