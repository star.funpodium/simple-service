var zlib = require('zlib');

//var c = require('net').connect(8582,'112.121.70.71', function(){
var c = require('net').connect(8582, function () {

	var buffer;

	c.setEncoding('utf8'); // commandline use utf8 encode
	c.on('data', function (d) {
		buffer = new Buffer(d, 'base64');
		zlib.unzip(buffer, function (err, decoded) {
			if (!err) {
				console.log('[receive] <= ' + decoded.toString());
			}
		});
	});
	c.on('close', function (err) {
		console.log('socket closed ' + err);
	});

	var cmds = [
		//{ auth : { uid : 1,  authkey : '28c857ea2d5b5528268f8d0c33d45455'} },		

		//{ broadcast : { txt : 'broadcast' } }		
		//{ auth : { uid : 2,  authkey : 'abbbfe28eca8db90c9a459f3bcd31209'} },	
		//{ auth : { uid : 3,  authkey : 'a17661f25af2f0a014fabb976588be22'} },	
		//{ auth : { uid : 4,  authkey : 'd45b51f0682262d05eea7a6549dd4f39'} },		
		//{ auth : { uid : 5,  authkey : 'c19c88443b18687be9b5f905a914c601'} },				
		//{ auth : { uid : 6,  authkey : 'c284397dcb2ce4fbdee155eab3c7c6ca'} },				
		//{ auth : { uid : 7,  authkey : '3e19c55d692118ff0c0ce31892ee71ae'} },				
		//{ login : { uid : 2 } },
		//{ privateUser : { name : '左右左右', txt : 'test' }},
		//{ warBroadcast : { msg : 'broadcast' } }		
		//{ initTeamBattle : {} },
		//{ joinDefenderTeamBattleWar : { point : 1, userData : '[ { "uid" : 1 }]' } }
		//{ writeTeamBattleLog : {battleLog : 'demo', point : 1, isWin : 1, attacker: '', defender :''} }
	];

	var runCmds = function (cmds, i) {
		setTimeout(function () {
			if (i < cmds.length) {
				var cmd = cmds[i];
				console.log('[send] => %s', JSON.stringify(cmd));
				zlib.gzip(JSON.stringify(cmd) + '\n', function (err, encode) {
					if (!err) {
						c.write(encode.toString('base64'));
						runCmds(cmds, i + 1);
					}
				});
				//c.write(JSON.stringify(cmd) + '\n');
				//runCmds(cmds, i+1);
			}

		}, 500);
	};
	runCmds(cmds, 0);
});