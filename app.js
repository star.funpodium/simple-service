"user strict";

let
	RouterManage = require('./router/routerManage'),
	EventEmitter = require('events').EventEmitter
const classPath = 'app.js'
let App = function (express, multer, logger) {
	let self = this;
	new RouterManage(express, multer)
	self.on('uncaughtException', function (err) {
		logger.error(err, classPath, 'App.uncaughtException');
	});

	self.on('loadComplete', function (message) {
		logger.info(message, classPath, 'App.loadComplete');
	})
	this.close = () => {
		setTimeout(() => {
			process.exit();
		}, 0);
	}
	process.on('SIGINT', () => {
		// stop after 10 secs, close all services & connections
		self.close()
	});
};

module.exports = App;
App.prototype.__proto__ = EventEmitter.prototype;