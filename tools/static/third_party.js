"use strict";

require('../base');
let Third_Party = require('../../data/third_party');

let MailStatic = function (model) {
	let self = this;
	this.list = [];
	this.third_party = Third_Party;
	this.model = model;

	this.load = function () {

		Object.keys(this.third_party.Channel).forEach(function (key) {
			let elem = {
				'id': self.third_party.Channel[key],
				'channel': key
			}

			self.model.Third_party.upsert(elem
			).catch(function (err) {
				console.log(err);
			});
		});
	}
};

module.exports = MailStatic;