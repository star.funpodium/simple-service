"user strict";
const sampleService = require('../../service/sample')

exports.Sample = {
	meta: {
		cache: true //use cache
	},
	method: 'get',
	path: '/v1/samples',
	runner: async function () {
		let result = await sampleService.findAll()
		return result
	}
}