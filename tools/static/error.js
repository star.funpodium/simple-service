"use strict";

let Base = require('../base');

let ErrorStatic = function (model) {
	let self = this;
	this.list = [];
	this.model = model;
	this.base = new Base();

	this.base.on('line', (line) => {
		let flag = /^.*-[0-9]{0,}.*$/.test(line);
		if (flag) {
			let errCodeReg = /\d+/;
			let codeReg = errCodeReg.exec(line);

			//let reg = /[/]{2}([a-zA-Z]|[\u4e00-\u9fa5]){1,}/;
			let errMsgReg = /[/]{2}([a-zA-Z]|[\u4e00-\u9fa5]){1,}/;
			let msgReg = errMsgReg.exec(line);
			let message = msgReg[0].split("//");

			let obj = {};
			obj.id = codeReg[0];
			obj.msg = message[1];
			obj.type = '1';

			self.list.push(obj);
		}
	});

	this.base.on('close', () => {
		self.model.Error_msg.destroy({
			where: {}
		}).then(function () {
			self.list.forEach(function (elem) {
				self.model.Error_msg.create(elem
				).catch(function (err) {
					console.log(err);
				});
			});
		}).catch(function (err) {
			console.log(err);
		});
	});

	this.load = function () {
		let path = '../share/utils/wrong.js'
		this.base.load(path);
	}
};

module.exports = ErrorStatic;