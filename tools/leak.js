"user strict";

const Fs = require('fs'),
	Profiler = require('v8-profiler-node8');
require('../runner')
let count = 0;

//let leakArray = [];
//function emptyObject() {}

//exports.TestABC = function (modules, req, res, next) {
//	leakArray.push(new emptyObject());
//	res.send(modules.formatter.encodeSuccessResult({}));
//};

const memory = () => {
	if (count >= 3)
		clearInterval(interval);

	let snapshot = Profiler.takeSnapshot();
	snapshot.export(function (err, result) {
		let name = "snapshot" + count + ".heapsnapshot";
		Fs.writeFileSync(name, result);
		snapshot.delete();
	});

	count++;
	return memory;
};

const interval = setInterval(memory(), 30000);