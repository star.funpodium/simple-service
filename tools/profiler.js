"user strict";

const Fs = require('fs'),
	Profiler = require('v8-profiler-node8');
require('../runner')
//function slow(factor) {
//	for (let i = 0; i < Math.pow(10, factor); ++i) {}
//}

//exports.TestABC = function (modules, req, res, next) {
//	slow(7);
//	res.send(modules.formatter.encodeSuccessResult({}));
//};

Profiler.startProfiling('profile_test', true);
setTimeout(() => {
	let profiling = Profiler.stopProfiling();
	profiling.export()
		.pipe(Fs.createWriteStream('profile.cpuprofile'))
		.on('finish', function () {
			profiling.delete();
		});
}, 30000);