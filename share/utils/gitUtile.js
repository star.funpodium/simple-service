let { execSync } = require('child_process')
const gitCommand = "git rev-parse HEAD"

module.exports.getGitCommitHash = () => {
    return execSync(gitCommand).toString().trim()
}

