"use strict";

let EventEmitter = require('events').EventEmitter;
let Fs = require('fs'),
	Readline = require('readline');

let GenerateBase = function () {
	let self = this;

	this.load = function (path) {
		let inputStream = Fs.createReadStream(path);

		let lineReader = Readline.createInterface({ input: inputStream });

		lineReader.on('line', function (line) {
			self.emit('line', line);
		});

		lineReader.on('close', function () {
			self.emit('close');
		});

		inputStream.on('error', function (err) {
			console.log(err);
		});
	}
}

module.exports = GenerateBase;
GenerateBase.prototype.__proto__ = EventEmitter.prototype;