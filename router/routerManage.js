"use strict";
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger/index')
const path = require('path')
const fs = require('fs')
const apiFolderPath = "../api"
const middlewareFolderPath = '../middleware'
const formatter = require('../share/utils/formatter')
const gitUtil = require('../share/utils/gitUtile')
const classPath = 'router/routerManage.js'
const error = require('../error')
function recFindByExt(base, ext, files, result) {
	files = files || fs.readdirSync(base)
	result = result || []

	files.forEach(
		function (file) {
			var newbase = path.join(base, file)
			if (fs.statSync(newbase).isDirectory()) {
				result = recFindByExt(newbase, ext, fs.readdirSync(newbase), result)
			}
			else {
				if (file.substr(-1 * (ext.length + 1)) == '.' + ext) {
					result.push(newbase)
				}
			}
		}
	)
	return result
}
var routerManage = function (router, multer) {

	let self = this;
	this.router = router;
	let middlewares = []

	this.loadMiddlewares = function (ms) {
		for (let key in ms) {
			let middleware = ms[key]
			middlewares.push(middleware)
		}
	}
	this.load = function (apis) {
		for (let key in apis) {
			let api = apis[key]
			let baseRouterHandler = async function (req, res, next) {
				try {
					let result = await api.runner(req, res, next)
					if (result != null) {
						res.send(formatter.encodeSuccessResult(result, res.logger));
						res.logger.info("API_END", classPath, 'baseRouterHandler')
					}
				} catch (err) {
					next(err)
				}
			}
			let apiMiddlewareFuncs = middlewares.map(elem => {
				return function (req, res, next) {
					elem.runner.apply({ apiKey: key }, [api.meta, req, res, next])
				}
			})
			if (api.method == 'post' && api.meta && Object.prototype.hasOwnProperty.call(api.meta, 'multer')) {
				self.router.post(api.path, ...apiMiddlewareFuncs, multer[api.meta.multer.type](api.meta.multer.name), baseRouterHandler)
			} else {
				self.router[api.method](api.path, ...apiMiddlewareFuncs, baseRouterHandler)
			}
		}
	}
	if (process.env.SWAGGER == 'true') {
		swaggerDocument.host = process.env.API_DOMAIN
		this.router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
		// this.router.get('/StatusCode', function (req, res) {
		// 	let statusCodes = wrong.Message.Error;
		// 	let keys = Object.keys(statusCodes)
		// 	let list = keys.map(elem => {
		// 		let obj = {}
		// 		obj.code = statusCodes[elem].code
		// 		obj.msg = statusCodes[elem].msg
		// 		return obj
		// 	})
		// 	res.render('status_code', { 'title': "Status Code", 'list': list })
		// })
	}

	this.router.get('/', async function (req, res) {
		// let redisStatus = redis.isConnected()
		// let mongoStatus = mongo.isConnected()
		// let mysqlStatus = await mysql.isConnected()
		// let rabbitmqStatus = await rabbitMQ.isConnected()
		let list = []
		// list.push({ name: 'Redis', status: redisStatus })
		// list.push({ name: 'Mongo', status: mongoStatus })
		// list.push({ name: 'Mysql', status: mysqlStatus })
		// list.push({ name: 'RabbitMQ', status: rabbitmqStatus })
		let commit = gitUtil.getGitCommitHash()
		res.render('device_connection_status', { 'title': "Device Connect Status", 'list': list, 'commit': commit })
	})

	let filePath = path.join(__dirname, middlewareFolderPath)
	let middlewareFiles = recFindByExt(filePath, 'js')
	middlewareFiles.forEach(elem => {
		this.loadMiddlewares(require(elem))
	})
	middlewares = middlewares.sort((a, b) => {
		return a.order - b.order
	})
	filePath = path.join(__dirname, apiFolderPath)
	let apiFiles = recFindByExt(filePath, 'js')
	apiFiles.forEach(elem => {
		this.load(require(elem))
	})
	this.router.use(function (req, res, next) {
		next(error.routerError)
	});
	this.router.use(function (err, req, res, next) {
		if (Object.prototype.hasOwnProperty.call(err, 'isCustom')) {
			let languageId = req.headers.language_id
			let customError = formatter.encodeErrorResult(err, res.logger, languageId)
			res.status(err.httpStatus)
			res.send(customError)
		} else {
			res.status(500);
			res.send(formatter.encodeSystemResult(err, res.logger));
		}
		if (res.logger) {
			res.logger.info("API_END", classPath, 'routerErrorHandler')
		}
	});
};

module.exports = routerManage;