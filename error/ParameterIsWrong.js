class ParameterIsWrong extends Error {
    constructor() {
        super("传入参数错误")
        this.isCustom = true
        this.httpStatus = 400
        this.status = -200001
        this.msgList = {
            'zh': '传入参数错误',
            'en': 'parameter is wrong'
        }
    }
}
module.exports = ParameterIsWrong