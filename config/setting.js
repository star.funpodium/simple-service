'user strict';
let setting = {
    serverTimeout: '60s',
    loggerName: 'nsports-subscription-api',
    limits: {
        uploadFileSize: 2 * 1024 * 1024, //Maximum file size is 2MB
    }
};
module.exports = setting