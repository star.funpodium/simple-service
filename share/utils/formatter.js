"use strict";
const classPath = 'share/utils/formatter.js'
const loggerHandler = require('../../module/loggerHandler')
const loggerName = require('../../config/setting').loggerName
const loggerOut = new loggerHandler(loggerName)
module.exports = {
	encodeSuccessResult: (data, logger) => {
		let res = {
			'Status': 1,
			'ErrMsg': "",
			'Data': data
		};
		logger = logger || loggerOut
		logger.debug(res, classPath, 'encodeSuccessResult')
		return res;
	},
	encodeErrorResult: (err, logger, language = 'zh') => {
		let res = {
			'Status': err.status,
			'ErrMsg': err.msgList[language],
			'Data': {}
		};
		logger = logger || loggerOut
		logger.debug(res, classPath, 'encodeErrorResult')
		return res;
	},
	encodeSystemResult: (err, logger) => {
		let data = {}
		if (err.stack) {
			logger.error(err.stack, classPath, 'encodeSystemResult');
			if (process.env.IS_DEBUG == 'true') {
				data = { msg: err.stack };
			}
		}
		let res = {
			'Status': 500,
			'ErrMsg': 'System error',
			'Data': data
		};
		logger = logger || loggerOut
		logger.debug(res, classPath, 'encodeSystemResult')
		return res;
	}
}
