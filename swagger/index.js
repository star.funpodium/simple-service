
const fs = require("fs")
let obj = {
    "swagger": "2.0",
    "info": {
        "description": "API Document",
        "version": "1.0.0",
        "title": "National Sports Restful API"
    },
    "host": "18.182.218.232:8081",
    "basePath": "/",
    "externalDocs": {
        "description": "Status Code",
        "url": "/StatusCode"
    },
    "paths": {},
    "definitions": {}
}
let definitions = {}
var normalizedPath = require("path").join(__dirname, "./models");
fs.readdirSync(normalizedPath).forEach(function (file) {
    let item = require("./models/" + file)
    let keys = Object.keys(item)
    keys.forEach(elem => {
        definitions[elem] = item[elem]
    })
});
let paths = {}
normalizedPath = require("path").join(__dirname, "./apis");
fs.readdirSync(normalizedPath).forEach(function (file) {
    let item = require("./apis/" + file)
    let keys = Object.keys(item)
    keys.forEach(elem => {
        paths[elem] = item[elem]
    })
});
obj.definitions = definitions
obj.paths = paths
module.exports = obj