"user strict";

let log4js = require('log4js');
function stringify(msg) {
	if (typeof msg == 'object') {
		msg = JSON.stringify(msg)
	}
	return msg
}
let loggerHandler = function (loggerName, customField) {

	customField = customField || {}
	let fields = {
		serviceName: loggerName || '',
		env: process.env.ENV || ''
	}
	log4js.configure({
		appenders: {
			console: { type: 'console' },
			logstash: {
				type: 'log4js-logstash-tcp',
				host: process.env.LOGSTASH_HOST,
				port: parseInt(process.env.LOGSTASH_PORT),
				fields: fields
			}
		},
		categories: {
			default: { appenders: ['logstash', 'console'], level: process.env.LOG_LEVEL }
			// default: { appenders: ['console'], level: process.env.LOG_LEVEL }
		}
	})

	let logger = log4js.getLogger(loggerName);
	function processFields(path, funcName, fields) {
		fields = fields || {}
		fields.path = path || ''
		fields.funcName = funcName || ''
		fields = Object.assign({}, customField, fields)
		return fields
	}
	this.info = function (msg, path, funcName, fields) {
		fields = processFields(path, funcName, fields)
		msg = stringify(msg)
		logger.info(msg, fields);
	};

	this.warn = function (msg, path, funcName, fields) {
		fields = processFields(path, funcName, fields)
		msg = stringify(msg)
		logger.warn(msg, fields);
	};

	this.error = function (msg, path, funcName, fields) {
		fields = processFields(path, funcName, fields)
		let err = stringify(msg)
		if (err == '{}')
			logger.error(msg, fields);
		else
			logger.error(err, fields);
	};

	this.debug = function (msg, path, funcName, fields) {
		fields = processFields(path, funcName, fields)
		if (process.env.IS_DEBUG == 'true') {
			msg = stringify(msg)
			logger.debug(msg, fields);
		}
	};

	this.process = function (body, res, next) {
		if (Object.prototype.hasOwnProperty.call(body, 'Status') &&
			Object.prototype.hasOwnProperty.call(body, 'ErrMsg') &&
			Object.prototype.hasOwnProperty.call(body, 'Data')) {
			res.send(body);
		} else {
			next(body);
		}
	};

	this.shutdown = function () {
		log4js.shutdown(function () { });
	};
};

module.exports = loggerHandler;
