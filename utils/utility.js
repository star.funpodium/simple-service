"user strict";
const uuidv1 = require('uuid/v1');
const uuidv3 = require('uuid/v3');
const uuidv4 = require('uuid/v4');
const uuidv5 = require('uuid/v5');
const crypto = require('crypto')
module.exports = {
	'desDecrypt': (text, key) => {
		key = key.length >= 8 ? key.slice(0, 8) : key.concat('0'.repeat(8 - key.length))
		const keyHex = new Buffer.from(key)
		const cipher = crypto.createDecipheriv('des-cbc', keyHex, keyHex)
		let c = cipher.update(text, 'base64', 'utf8')
		c += cipher.final('utf8')
		return c
	},
	'randomString': function (len, format) {
		var text = '';
		for (var i = 0; i < 5; i++) {
			text += format.charAt(Math.floor(Math.random() * format.length));
		}

		return text;
	},

	'convertTimestampToDateString': function (timestamp) {
		var date = new Date(timestamp * 1000);
		var year = date.getFullYear();
		var month = String(date.getMonth() + 1);
		if (month.length == 1)
			month = '0' + month;

		var day = String(date.getDate());
		if (day.length == 1)
			day = '0' + day;

		var hour = String(date.getHours());
		if (hour.length == 1)
			hour = '0' + hour;

		var min = String(date.getMinutes());
		if (min.length == 1)
			min = '0' + min;

		var sec = String(date.getSeconds());
		if (sec.length == 1)
			sec = '0' + sec;

		return year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
	},

	'convertTimestampToUTCDateString': function (timestamp) {
		var date = new Date(timestamp * 1000);
		var year = date.getUTCFullYear();
		var month = String(date.getUTCMonth() + 1);
		if (month.length == 1)
			month = '0' + month;

		var day = String(date.getUTCDate());
		if (day.length == 1)
			day = '0' + day;

		var hour = String(date.getUTCHours());
		if (hour.length == 1)
			hour = '0' + hour;

		var min = String(date.getUTCMinutes());
		if (min.length == 1)
			min = '0' + min;

		var sec = String(date.getUTCSeconds());
		if (sec.length == 1)
			sec = '0' + sec;

		return year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
	},

	'convertDateStringToTimestamp': function (date) {
		return Math.round(new Date(date).getTime() / 1000);
	},

	'returnNextYear': function (timestamp) {
		return timestamp + 31536000;
	},

	'returnPreviousYear': function (timestamp) {
		return timestamp - 31536000;
	},

	'returnNextDay': function (timestamp) {
		return timestamp + 86400;
	},

	'returnNextMonth': function (timestamp) {
		return timestamp + 2592000;
	},

	'returnPreviousDay': function (timestamp) {
		return timestamp - 86400;
	},

	'returnDaysBefore': function (timestamp, days) {
		return timestamp - (days * 86400);
	},

	'returnDaysAfter': function (timestamp, days) {
		return timestamp + (days * 86400);
	},

	'returnNowTime': function (date) {
		//UTC + 0
		var utcTimestamp = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
			date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());

		return Math.round(utcTimestamp / 1000);
	},

	'returnStartOfToday': function () {
		let date = new Date()
		date.setHours(0)
		date.setMinutes(0)
		date.setSeconds(0)
		date.setMilliseconds(0)
		return Math.round(date.getTime() / 1000);
	},

	'isDateZero': function (date) {
		return date.toString() === 'Invalid Date' || date.getTime() === 0;
	},

	'checkUid': function (uid) {
		return /^\d+$/.test(uid);
	},

	'isNumber': function (input) {
		return typeof (input) === 'number';
	},

	'isArrayType': function (obj) {
		return Object.prototype.toString.call(obj) === '[object Array]';
	},

	'isUndefined': function (obj) {
		return Object.prototype.toString.call(obj) === '[object Undefined]';
	},

	'isObject': function (obj) {
		return Object.prototype.toString.call(obj) === '[object Object]'
	},

	'random': function (min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	},

	'isRandomIn': function (percentage) {
		return (this.random(0, 99) < percentage) ? true : false;
	},

	'isEqual': function (source, dest) {
		return (source == dest) ? true : false;
	},

	'isObjectEmpty': function (obj) {
		return Object.keys(obj).length === 0;
	},

	'getFirstDayOfWeek': function (now, firstDayOfWeek) {
		firstDayOfWeek = firstDayOfWeek || 1;
		var nowDate = now.clone().clearTime();

		return nowDate.addDays(-1 * (nowDate.getDay() - firstDayOfWeek + 7) % 7);
	},

	'getLastDayOfWeek': function (now, firstDayOfWeek) {
		firstDayOfWeek = firstDayOfWeek || 1;
		var nowDate = now.clone().clearTime();

		return nowDate.addDays((firstDayOfWeek + 6 + 7 - nowDate.getDay()) % 7);
	},


	'generatingRandomCode': function (len) {
		len = (len <= 0) ? 1 : len;
		var text = '';
		var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

		for (var i = 0; i < len; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	},

	'generatingRandomInteger': function (len) {
		len = (len <= 0) ? 1 : len;
		var text = '';
		var possible = '0123456789';

		for (var i = 0; i < len; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	},

	'generatingTestAccount': function () {
		var random = Math.random().toString(36).substr(2);
		var account = random.toUpperCase();

		return account;
	},

	'generatUUIDV1': function () {
		return uuidv1();
	},

	'generatUUIDV3': function (namespace) {
		return uuidv3(namespace, uuidv3.URL);
	},

	'generatUUIDV4': function () {
		return uuidv4();
	},

	'generatUUIDV5': function (namespace) {
		return uuidv5(namespace, uuidv5.URL);
	},

	'isMatch': function (str) {
		var format = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/;

		return (format.test(str)) ? true : false;
	},

	'checkEmail': function (str) {
		var format = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;

		return (format.test(str)) ? true : false;
	},

	'checkAccount': function (str) {
		var format = /^[a-zA-Z]{1}[a-zA-Z0-9_]{7,29}$/;
		return (format.test(str)) ? true : false;
	},

	'checkPassword': function (str) {
		var regp = /^(?=.*[a-zA-Z]+)(?=.*[0-9]+)[a-zA-Z0-9]+$/;

		return (!regp.test(str) || str.length < 8 || str.length > 16) ? false : true
	},

	'checkNickName': function (str) {
		let regp = /^[0-9a-zA-Z\u4E00-\u9FA5]{2,10}$/;
		return regp.test(str)
	},

	'ckeckIsPhone': function (str) {
		var regp = /^[0-9]+$/;
		return (!regp.test(str) || str.length > 11) ? false : true
	},

	'separate': function (text) {
		let queue = [];
		let limit = 0;
		let tokenize = text.split(" ");
		let self = this
		tokenize.forEach(function (elem) {
			if (limit > 3)
				return;

			if (elem == '')
				return;

			let flag = self.isMatch(elem)
			if (flag == true)
				return;

			limit++;
			queue.push(elem);
		});

		return queue;
	},

	'addObjectToContainer': function (source, elem) {
		source.push(elem);
	},

	'keyValueArrayToObject': function (array) {
		let result = array.reduce((acc, cur) => {
			acc[cur.key] = cur.value
			return acc
		}, {})
		return result
	},

	'cloneObject': function (object) {
		return JSON.parse(JSON.stringify(object))
	},
	'processEntity': async function (entity) {
		let self = this
		let attribute = await entity.getEntity_attributes()
		let result = JSON.parse(JSON.stringify(entity))
		result = Object.assign({}, self.keyValueArrayToObject(attribute), result)
		return result
	},
	'processEntityWithAttribute': function (entity) {
		let self = this
		let result = JSON.parse(JSON.stringify(entity))
		delete result.entity_attributes
		let attribute = entity.entity_attributes
		result = Object.assign({}, self.keyValueArrayToObject(attribute), result)
		return result
	},
	'retUniqueItemsFromArray': function (array, count) {
		// Make a copy of the array
		var tmp = array.slice(array);
		var ret = [];

		for (var i = 0; i < count; i++) {
			if (tmp.length <= 0)
				break;

			var index = Math.floor(Math.random() * tmp.length);
			var removed = tmp.splice(index, 1);
			// Since we are only removing one element
			ret.push(removed[0]);
		}
		return ret;
	},

	'retClientIpAddress': function (req) {
		return (req.headers["X-Forwarded-For"] ||
			req.headers["x-forwarded-for"] ||
			'').split(',')[0] ||
			req.client.remoteAddress;
	},

	'processILike': function (article, uid) {
		article.i_like = false
		article.i_dislike = false
		if (Object.prototype.hasOwnProperty.call(article, 'user_likes') && uid != null) {
			article.user_likes.forEach(obj => {
				if (obj.uid == uid) {
					if (obj.be_like == 0) {
						article.i_like = true
					} else {
						article.i_dislike = true
					}
				}
			})
			delete article.user_likes
		}
		return article
	},

	'generalUniqueGuid': function (guidList) {
		let breakPoint = false
		let guid;
		do {
			var randomNum = Math.random().toString(10).slice(2)
			guid = randomNum.substring(0, 4)
			if (guidList.indexOf(guid) === -1) {
				breakPoint = true
			}
		} while (!breakPoint)
		return guid
	},

	'checkImgUrl': function (imgUrl) {
		var regex = /(^http|(s):\/\/)(.+?)(\.jpeg|\.jpg|\.gif|\.png)$/g
		var isValid = regex.test(imgUrl)
		return isValid
	},

	'shuffle': function (array) {
		var currentIndex = array.length, temporaryValue, randomIndex;
		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;
			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}
		return array;
	},
	'wait': function (milliseconds) {
		return new Promise(reslove => {
			setTimeout(() => {
				reslove()
			}, milliseconds)
		})
	},

	'faltten': function flatten(obj) {//faltten nested obj 
		/*
		sample: 
		input:
			{
				nest:{
					name:"Aaa"
				}
			}
		
		output:
			{
				"nest.name":"Aaa"
			}
		*/
		function dive(currentKey, input, target) {
			for (let i in input) {
				if (input[i]) {
					let newKey = i
					let newVal = input[i]

					if (currentKey.length > 0) {
						newKey = currentKey + '.' + i
					}

					// if (typeof newVal === 'object' && !Array.isArray(newVal)) { //parse obj only
					if (typeof newVal === 'object') { //parse array / obj
						dive(newKey, newVal, target)
					} else {
						target[newKey] = newVal
					}
				}
			}
		}
		let result = {}
		dive("", obj, result)
		return result
	}
}