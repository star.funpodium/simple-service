"user strict";
"user strict";

const EnvSetter = require('../share/envSetter')
EnvSetter()
const sinon = require('sinon')
const expect = require('chai').expect
const assert = require('chai').assert
const request = require('supertest')
const authMiddleware = require('../middleware/auth').needLogin
const sandbox = sinon.createSandbox()
const server = require('../runner')
const rewire = require('rewire')
const articleDao = require('../daoModel/mongo/article')
const articleCache = require('../daoModel/cache/article')

describe('#api 取得文章詳目', function () {
    const newsService = rewire('../service/news')
    beforeEach(function () {
        sandbox.stub(authMiddleware, 'runner').callsFake((meta, req, res, next) => {
            return next()
        })
    });

    afterEach(function (done) {
        sandbox.restore();
        done()
    })

    it('test detail [non cache] [audio]', async function () {
        const articleId = `5a4b7ad4-06a0-4c03-a92a-d8396aa79c9b`
        const uid = 2
        const shortUrl = null

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isDetailExist').resolves(false)
        sandbox.stub(articleDao, 'findById').resolves(fakeRawData.audio[0])
        sandbox.stub(articleCache, 'saveDetail').resolves()

        let checkFun = newsService.__get__('getNewsDetail')
        let result = await checkFun({ uid, articleId, shortUrl })
        verifyColumn(result, { testDetail: true, testAudio: true })
        assert.ok(true)
    })
    it('test detail [non cache] [normal]', async function () {
        const articleId = `5a4b7ad4-06a0-4c03-a92a-d8396aa79c9b`
        const uid = 2
        const shortUrl = null

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isDetailExist').resolves(false)
        sandbox.stub(articleDao, 'findById').resolves(fakeRawData.normal[0])
        sandbox.stub(articleCache, 'saveDetail').resolves()

        let checkFun = newsService.__get__('getNewsDetail')
        let result = await checkFun({ uid, articleId, shortUrl })
        verifyColumn(result, { testDetail: true, testAudio: false })
        assert.ok(true)
    })
    it('test detail [has cache] [audio]', async function () {
        const articleId = `5a4b7ad4-06a0-4c03-a92a-d8396aa79c9b`
        const uid = 2
        const shortUrl = null

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isDetailExist').resolves(true)
        sandbox.stub(articleCache, 'getDetail').resolves(fakeRawData.audio[0])

        let checkFun = newsService.__get__('getNewsDetail')
        let result = await checkFun({ uid, articleId, shortUrl })
        verifyColumn(result, { testDetail: true, testAudio: true })
        assert.ok(true)
    })
    it('test detail [has cache] [normal]', async function () {
        const articleId = `5a4b7ad4-06a0-4c03-a92a-d8396aa79c9b`
        const uid = 2
        const shortUrl = null

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isDetailExist').resolves(true)
        sandbox.stub(articleCache, 'getDetail').resolves(fakeRawData.normal[0])

        let checkFun = newsService.__get__('getNewsDetail')
        let result = await checkFun({ uid, articleId, shortUrl })
        verifyColumn(result, { testDetail: true, testAudio: false })
        assert.ok(true)
    })
})

describe('#api 取得下層留言列表', function () {
    const newsService = rewire('../service/news')
    beforeEach(function () {
        sandbox.stub(authMiddleware, 'runner').callsFake((meta, req, res, next) => {
            return next()
        })
    })
    afterEach(function () {
        sandbox.restore()
    })

    it('test news list [non cache] [audio]', async function () {
        const sportType = 'football'
        const type = 'newest'
        const limit = 3
        const offset = 0
        const isPintop = false
        const league = 'nba'
        const entityIds = []
        const mediaFilter = 'audio'

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isListExist').resolves(false)
        sandbox.stub(articleDao, 'findNewsList').resolves(fakeRawData.audio)
        sandbox.stub(articleDao, 'countAmount').resolves(100)
        sandbox.stub(articleCache, 'saveList').resolves()

        let checkFun = newsService.__get__('getNewsListV2')
        let result = await checkFun({ sportType, type, limit, offset, isPintop, league, entityIds, mediaFilter })
        let list = result.articles
        list.forEach(ele => {
            verifyColumn(ele, { testDetail: false, testAudio: false })
        })
        assert.ok(true)
    })

    it('test news list [has cache] [audio]', async function () {
        const sportType = 'football'
        const type = 'newest'
        const limit = 3
        const offset = 0
        const isPintop = false
        const league = 'nba'
        const entityIds = []
        const mediaFilter = 'audio'

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isListExist').resolves(true)
        sandbox.stub(articleCache, 'getList').resolves({
            idx: ['AAA'],
            totalCount: 100
        })
        sandbox.stub(articleCache, 'getDetail').resolves(fakeRawData.audio[0])

        let checkFun = newsService.__get__('getNewsListV2')
        let result = await checkFun({ sportType, type, limit, offset, isPintop, league, entityIds, mediaFilter })
        let list = result.articles
        list.forEach(ele => {
            verifyColumn(ele, { testDetail: false, testAudio: false })
        })
        assert.ok(true)
    })

    it('test news list [non cache] [normal]', async function () {
        const sportType = 'football'
        const type = 'newest'
        const limit = 3
        const offset = 0
        const isPintop = false
        const league = 'nba'
        const entityIds = []
        const mediaFilter = 'normal'

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isListExist').resolves(false)
        sandbox.stub(articleDao, 'findNewsList').resolves(fakeRawData.normal)
        sandbox.stub(articleDao, 'countAmount').resolves(100)
        sandbox.stub(articleCache, 'saveList').resolves()

        let checkFun = newsService.__get__('getNewsListV2')
        let result = await checkFun({ sportType, type, limit, offset, isPintop, league, entityIds, mediaFilter })
        let list = result.articles
        list.forEach(ele => {
            verifyColumn(ele, { testDetail: false, testAudio: false })
        })
        assert.ok(true)
    })

    it('test news list [has cache] [normal]', async function () {
        const sportType = 'football'
        const type = 'newest'
        const limit = 3
        const offset = 0
        const isPintop = false
        const league = 'nba'
        const entityIds = []
        const mediaFilter = 'normal'

        const fakeRawData = require('./fakeData/news.json')
        sandbox.stub(articleCache, 'isListExist').resolves(true)
        sandbox.stub(articleCache, 'getList').resolves({
            idx: ['AAA'],
            totalCount: 100
        })
        sandbox.stub(articleCache, 'getDetail').resolves(fakeRawData.normal[0])

        let checkFun = newsService.__get__('getNewsListV2')
        let result = await checkFun({ sportType, type, limit, offset, isPintop, league, entityIds, mediaFilter })
        let list = result.articles
        list.forEach(ele => {
            verifyColumn(ele, { testDetail: false, testAudio: false })
        })
        assert.ok(true)
    })
})
function verifyColumn(data, { testDetail = true, testAudio = true }) {
    const newsKeysIdx = [
        'article_id',
        'title',
        'author',
        'allow_reply',
        'content',
        'source',
        'published_at',
        'timestamp',
        'category',
        'commentTotalCount',
        'likes',
        'dislikes',
        'i_like',
        'i_dislike',
        'image',
        'league',
        'similar_articles',
        'shorturl',
        'audio'
    ]
    const typeIdx = {
        'article_id': 'string',
        'title': 'string',
        'author': 'object',
        'allow_reply': 'boolean',
        'content': 'array',
        'source': 'string',
        'published_at': 'string',
        'timestamp': 'number',
        'category': 'string',
        'commentTotalCount': 'number',
        'likes': 'number',
        'dislikes': 'number',
        'i_like': 'boolean',
        'i_dislike': 'boolean',
        'image': 'string',
        'league': 'string',
        'similar_articles': 'array',
        'shorturl': 'string',
        'audio': 'object'
    }

    if (!testDetail) {
        newsKeysIdx.splice(newsKeysIdx.indexOf('author'), 1)
        newsKeysIdx.splice(newsKeysIdx.indexOf('allow_reply'), 1)
        newsKeysIdx.splice(newsKeysIdx.indexOf('content'), 1)
        newsKeysIdx.splice(newsKeysIdx.indexOf('dislikes'), 1)
        newsKeysIdx.splice(newsKeysIdx.indexOf('i_like'), 1)
        newsKeysIdx.splice(newsKeysIdx.indexOf('i_dislike'), 1)
        newsKeysIdx.splice(newsKeysIdx.indexOf('similar_articles'), 1)
        delete typeIdx['author']
        delete typeIdx['allow_reply']
        delete typeIdx['content']
        delete typeIdx['dislikes']
        delete typeIdx['i_like']
        delete typeIdx['i_dislike']
        delete typeIdx['similar_articles']
    }

    expect(data).to.contains.keys(
        newsKeysIdx
    )

    for (let i in newsKeysIdx) {
        let key = newsKeysIdx[i]
        let type = typeIdx[newsKeysIdx[i]]
        expect(data[key]).to.be.a(type)
    }

    if (testDetail) checkAuthor(data['author'])
    if (testAudio) checkAudio(data['audio'], { testAudio })
    return

    function checkAuthor(author) {
        const authorKeysIdx = [
            'id', 'name'
        ]
        expect(author).to.contains.keys(
            authorKeysIdx
        )
        expect(author['id']).to.be.a('string')
        expect(author['name']).to.be.a('string')
    }
    function checkAudio(audio, { testAudio }) {
        const audioKeysIdx = [
            'url', 'length'
        ]
        expect(audio).to.contains.keys(
            audioKeysIdx
        )
        expect(audio['length']).to.be.a('number')
        expect(audio['url']).to.be.a('string')
        if (testAudio) {
            expect(audio['length']).not.to.deep.equal(-1)
            expect(audio['url']).not.to.deep.equal('')
        }
    }
}