"user strict";

const util = require('util'),
	bodyParser = require('body-parser'),
	Express = require('express'),
	Timeout = require('connect-timeout'),
	config = require('./config/setting'),
	App = require('./app'),
	LoggerHandler = require('./module/loggerHandler'),
	logger = new LoggerHandler(config.loggerName),
	Multer = require('multer'),
	cors = require('cors'),
	requestIp = require('request-ip')
	;
require('morgan')
//connectDomain = require('connect-domain');

let rest = Express();
rest.use(bodyParser.text())
rest.use(bodyParser.json()); // for parsing application/json
rest.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
rest.use(cors())
rest.use(Timeout(config.serverTimeout));
rest.use(requestIp.mw())
rest.set('view engine', 'ejs');
const multer = Multer({
	storage: Multer.MemoryStorage,
	limits: {
		fileSize: config.limits.uploadFileSize
	}
});
let app
let Server = rest.listen(process.env.API_PORT, process.env.HTTP_SERVER_HOST, function () {
	app = new App(rest, multer, logger);
	app.emit('loadComplete', 'config => ');
	app.emit('loadComplete', util.inspect(config, true, null));
});

process.on('uncaughtException', function (err) {
	if (app) {
		app.emit('uncaughtException', err);
	} else {
		logger.error(err, 'runner.js', 'process.on(uncaughtException)')
	}
});

if (!('toJSON' in Error.prototype))
	Object.defineProperty(Error.prototype, 'toJSON', {
		value: function () {
			var alt = {};

			Object.getOwnPropertyNames(this).forEach(function (key) {
				alt[key] = this[key];
			}, this);

			return alt;
		},
		configurable: true,
		writable: true
	});
Server.on('close', () => {
	if (app) {
		app.close()
	}
})
module.exports = Server;
