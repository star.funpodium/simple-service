const classPath = 'middleware/paramProcess.js'
function processParam(param, reqParam, logger) {
	if (param.properties) {
		let keys = Object.keys(param.properties)
		for (let i = 0; i < keys.length; i++) {
			let key = keys[i]
			if (Object.prototype.hasOwnProperty.call(reqParam, key)) {
				if (param.properties[key].type != typeof reqParam[key]) {
					let result = null
					try {
						if (param.properties[key].type == 'number' && reqParam[key]) {
							result = Number(reqParam[key])
							if (!isNaN(result)) {
								reqParam[key] = result
							}
						}
						if (param.properties[key].type == 'array' && reqParam[key]) {
							result = JSON.parse(reqParam[key])
							reqParam[key] = result
						}
					} catch (err) {
						logger.info(err, classPath, 'processParam')
					}
				}
			}
		}
	}
}
exports.paramProcess = {
	order: 2,
	runner: async function (meta, req, res, next) {
		try {
			if (meta.param) {
				if (meta.param.query) {
					processParam(meta.param.query, req.query, res.logger)
				}
				if (meta.param.headers) {
					processParam(meta.param.headers, req.headers, res.logger)
				}
				if (meta.param.params) {
					processParam(meta.param.params, req.params, res.logger)
				}
				next()
			} else {
				next()
			}
		} catch (err) {
			next(err)
		}
	}
};