module.exports = {
    "env": {
        "commonjs": true,
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "ignorePatterns": ["node_modules/", "test/", "tools/"],
    "rules": {
        "no-unused-vars": ["error", { "args": "none" }]
    }
};